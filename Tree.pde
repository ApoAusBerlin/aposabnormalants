class Tree{
  int trunkheight = 10;
  int trunkwidth = 10;
  color trunkcolor = color(120, 50, 10);
  
  int crownwidth = 30;
  int crownheight = 20;
  color crowncolor = color(30, 180, 70);
  
  Tree(){
  }
  
  Tree(int treewidth, int treeheight){
    trunkwidth = treewidth / 3;
    trunkheight = treeheight / 3;
    crownwidth = treewidth;
    crownheight = treeheight - trunkheight;
  }
  
  int getTreewidth(){
    return crownwidth;
  }
  
  int getTreeheight(){
    return trunkheight + crownheight;
  }
  
  void drawTree(int treex, int treey){
    fill(trunkcolor);
    rect(treex - trunkwidth/2, treey - trunkheight, trunkwidth, trunkheight);
    fill(crowncolor);
    triangle(treex - crownwidth/2, treey - trunkheight, treex, treey - trunkheight - crownheight, treex + crownwidth/2, treey - trunkheight);    
  }
  
}
