class Ant{
  int antlength, antennalength, headlength, bodylength, abdomenlength;  
  int direction;
  int headx, heady;
  
  Ant(){
    antlength = 10;
    antennalength = antlength/10;
    headlength = antlength/10;
    bodylength = antlength/5;
    abdomenlength = antlength - antennalength - headlength - bodylength;
    headx = width/2;
    heady = height/2;
  }
  
  Ant(int templength){
    antlength = templength;
    headx = width/2;
    heady = height/2;
    direction = (int) random(4);
    int antennalength = antlength/10;
    int headlength = antlength/10;
    int bodylength = antlength/5;
    int abdomenlength = antlength - antennalength - headlength - bodylength;
  }
  
  Ant(int templength, int tempx, int tempy){
    antlength = templength;
    headx = tempx;
    heady = tempy;
    direction = (int) random(4);
    int antennalength = antlength/10;
    int headlength = antlength/10;
    int bodylength = antlength/5;
    int abdomenlength = antlength - antennalength - headlength - bodylength;
  }
  
  Ant(int templength, int tempx, int tempy, int tempdirection){
    antlength = templength;
    headx = tempx;
    heady = tempy;
    direction = tempdirection;
    int antennalength = antlength/10;
    int headlength = antlength/10;
    int bodylength = antlength/5;
    int abdomenlength = antlength - antennalength - headlength - bodylength;
  }
  
  void moveForward(){
    switch(direction){
      case 0:
        heady += antlength/2;
        break;
      case 1:
        headx += antlength/2;
        break;
      case 2:
        heady -= antlength/2;
        break;
      case 3:
        headx -= antlength/2;
        break;
      default:
        println("Invalid direction to move.");
        break;
    }
  }
  
  void turnLeft(){
    if(direction > 0){
      direction -= 1;
    } else {
      direction = 3;
    }
  }
  
  void turnRight(){
    if(direction < 3){
      direction += 1;
    } else {
      direction = 0;
    }
  }
  
  void drawAnt(){
    fill(0);
    ellipse(headx, heady, headlength, headlength);
    switch(direction){
      case 0:
        ellipse(headx, heady + headlength/2 + bodylength/2, bodylength/2, bodylength);
        ellipse(headx, heady + headlength/2 + bodylength/2 + abdomenlength/2, abdomenlength/2, abdomenlength);
        break;
      case 1:
        ellipse(headx - headlength/2 - bodylength/2, heady, bodylength, bodylength/2);
        ellipse(headx - headlength/2 - bodylength/2 - abdomenlength/2, heady, abdomenlength, abdomenlength/2);
        break;
      case 2:
        ellipse(headx, heady - headlength/2 - bodylength/2, bodylength/2, bodylength);
        ellipse(headx, heady - headlength/2 - bodylength/2 - abdomenlength/2, abdomenlength/2, abdomenlength);
        break;
      case 3:
        ellipse(headx + headlength/2 + bodylength/2, heady, bodylength, bodylength/2);
        ellipse(headx + headlength/2 + bodylength/2 + abdomenlength/2, heady, abdomenlength, abdomenlength/2);
        break;
      default:
        println("Error while drawing ant.");
        break;
    }
  }
    
   void moveIntoRandomDirection(){
    int directionDice = (int) random(3);
    if(directionDice == 0){
      turnLeft();
    } else if(directionDice == 2){
      turnRight();
    }
    moveForward();
    drawAnt();
  }
    
}
