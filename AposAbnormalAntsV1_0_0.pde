/*
 * A simulation of a tree, a forest of trees, a forest with ants
 * (c)2015 Apo
 * TODO: Rebuilt to 1. Size areas dynamically and 2. fill area after area
 */

int stagewidth = 600;
int stageheight = 400;
color stagebackgroundcolor = color(100, 130, 25);
int antcount = 1;
Ant[] forestant;


void setup(){
  size(stagewidth, stageheight);
  background(stagebackgroundcolor);
  
  drawTrees();
  forestant = new Ant[antcount];
  createAnts();
  // drawRuler(); 
}

void draw(){
  for(int i = 0; i < antcount; i++){
    forestant[i].moveIntoRandomDirection();
  } 
  //drawTrees();
}

void drawRuler(){
  line(width/2, 0, width/2, height);
  for(int i=1; i<height/10; i++){
    if(i % 2 == 0){
      line(width/2 - 10, i * 10, width/2 + 10, i * 10);
    } else {
      line(width/2 - 5, i * 10, width/2 + 5, i * 10);
    }
  }
  line(0, height/2, width, height/2);
  for(int i=1; i<width/10; i++){
    if(i % 2 == 0){
      line(i * 10, height/2 - 10, i * 10, height/2 + 10);
    } else {
      line(i * 10, height/2 - 5, i * 10, height/2 + 5);
    }
  }
}

void drawTrees(){
  int treecount = 100;
  int borderwidth = 100;
  
  Tree[] forest = new Tree[treecount];
  
  for(int i = 0; i < treecount; i++){
    int randomx, randomy = 0;
    randomx = (int) random(width);
    randomy = (int) random(height);
    
    while((randomx > borderwidth && randomx < (width - borderwidth)) && (randomy > borderwidth && randomy < (height - borderwidth))){
      randomx = (int) random(width);
      randomy = (int) random(height);
    }
      
    forest[i] = new Tree();
    forest[i].drawTree(randomx, randomy);
  } 
}

void createAnts(){
  for(int i=0; i < antcount; i++){
    forestant[i] = new Ant();
  }
}
  
